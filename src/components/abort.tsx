import { Component } from 'react';
import { Link } from 'react-router-dom';

class AbortPage extends Component {
    render() {
        return (
            <div>
                <h2>
                    Abort Page!
                </h2>
                <Link to="/">
                    <button>
                        Go to Home page!
                    </button>
                </Link>
            </div>
        );
    }
}

export default AbortPage;
