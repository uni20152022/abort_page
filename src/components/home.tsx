import { Component } from 'react';
import { Link } from 'react-router-dom';

import withAbortController from '../wrappers/with_abort_controller';

class HomePage extends Component<any, any> {
    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/photos', {
            signal: this.props.abortController.signal
        })
            .then(res => res.json())
            .then(json => console.log(json))
            .catch(error => console.log(error));
    }

    render() {
        return (
            <div>
                <h2>
                    Home Page!
                </h2>
                <Link to="/abort">
                    <button type="button">
                        Go to Abort page!
                    </button>
                </Link>
            </div>
        );
    }
}

const WrappedHomePage = withAbortController(HomePage);

export default WrappedHomePage;
