import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AbortPage from './components/abort';
import WrappedHomePage from './components/home';

function App() {
  return (
    <BrowserRouter basename="/">
      <Switch>
        <Route exact path="/" component={ WrappedHomePage }/>
        <Route exact path="/abort/" component={ AbortPage }/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
