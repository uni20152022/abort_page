import { Component } from 'react';

export default function withAbortController(MyComponent: any) {
    return class WithAbortController extends Component<{}, any> {
        constructor(props: any) {
            super(props);
            console.log('with_abort_controller');
            this.state = {
                abortController: new AbortController()
            };
        }

        componentWillUnmount() {
            console.log('about to abort', this.state.abortController);
            this.state.abortController.abort();
        }

        render() {
            console.log('rendering withAboutController');
            return <MyComponent abortController={ this.state.abortController }/>
        }
    }
}
